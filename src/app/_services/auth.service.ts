import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '@app/_interfaces/user';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environment/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('auth')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string) {
    const loginData = {email, password};
    console.log(loginData);
    return this.http.post<any>(`${environment.api}/users/authenticate`, loginData)
      .pipe(
        map(user => {
          localStorage.setItem('auth', JSON.stringify(user));
          this.currentUserSubject.next(user);
          return user;
        })
      );
  }

  signup(newUser: User) {
    return this.http.post<User>(`${environment.api}/users/signup`, {newUser});
  }

  logout() {
    localStorage.removeItem('auth');
    location.reload();
  }
}
