import {Injectable} from '@angular/core';
import {Alert} from '@app/_interfaces/alert';
import {AlertSeverity} from '@app/_enums/alert-severity.enum';
import {MessageService} from 'primeng/api';
import * as uniqid from 'uniqid';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private messageService: MessageService) {
  }

  success(summary: string, message: string) {
    const alert: Alert = {
      id: uniqid(),
      summary: summary,
      detail: message,
      severity: AlertSeverity.SUCCESS
    };
    this.sendAlert(alert);
  }

  info(summary: string, message: string) {
    const alert: Alert = {
      id: uniqid(),
      summary: summary,
      detail: message,
      severity: AlertSeverity.INFO
    };
    this.sendAlert(alert);
  }

  warning(summary: string, message: string) {
    const alert: Alert = {
      id: uniqid(),
      summary: summary,
      detail: message,
      severity: AlertSeverity.WARNING
    };
    this.sendAlert(alert);
  }

  error(summary: string, message: string) {
    const alert: Alert = {
      id: uniqid(),
      summary: summary,
      detail: message,
      severity: AlertSeverity.ERROR
    };
    this.sendAlert(alert);
  }

  sendAlert(alert: Alert) {
    this.messageService.add(alert);
  }

  clear(key?: string) {
    this.messageService.clear(key);
  }
}
