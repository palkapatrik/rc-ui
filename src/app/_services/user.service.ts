import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '@app/_interfaces/user';
import {environment} from '@environment/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.api}/users/`);
  }

  getUserById(id: string): Observable<User> {
    return this.http.get<User>(`${environment.api}/users/${id}`);
  }
}
