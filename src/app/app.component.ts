import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'rc-ui';

  constructor(private router: Router,
              private titleService: Title) {
  }

  setTitle(newTitle: string): void {
    this.titleService.setTitle(newTitle);
  }

  getTitle(state, parent) {
    const data = [];

    if (parent?.snapshot?.data?.title) {
      data.push(parent.snapshot.data.title);
    }

    if (state && parent) {
      data.push(...this.getTitle(state, state.firstChild(parent)));
    }
    return (data);
  }

  ngOnInit() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map(() => this.router)
      )
      .subscribe(() => {
        const title = this.getTitle(this.router.routerState, this.router.routerState.root).join(' | ');
        this.setTitle(title);
      });
  }
}
