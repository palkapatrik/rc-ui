import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {RepositoryService} from '@app/repositories/repository.service';
import {FormBuilder, Validators} from '@angular/forms';
import {Repository} from '@app/_interfaces/repository';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '@app/_services/user.service';
import {User} from '@app/_interfaces/user';
import {Subscription} from 'rxjs';
import {AuthService} from '@app/_services/auth.service';


@Component({
  selector: 'app-repository-edit',
  templateUrl: './repository-edit.component.html',
  styleUrls: ['./repository-edit.component.sass']
})
export class RepositoryEditComponent implements OnInit, OnDestroy {
  @Output() created: EventEmitter<void> = new EventEmitter<void>();
  isNew: boolean;
  subscriptions: Subscription[] = [];

  id: string;
  repo: Repository;
  repoOwner: User = {fullName: 'Who will own this repo?'};
  users: User[] = [];

  repositoryForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(5)]],
    owner: [''],
    description: ['']
  });

  constructor(private repositoryService: RepositoryService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private userService: UserService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.isNew = !this.route.snapshot.params.id;
    this.subscriptions.push(this.route.params
      .subscribe(params => this.isNew = !params.id)
    );

    if (!this.isNew){
      this.loadRepoDetail();
    }

    this.loadUsers();
    // TODO load owner name from loaded users
  }

  getIdFromRoute() {
    this.route.params.subscribe(params => {
      this.id = params.id;
    });
  }

  loadRepoDetail() {
    this.getIdFromRoute();
    this.subscriptions.push(
      this.repositoryService.getDetail(this.id)
        .subscribe({
          next: data => this.repo = data,
          complete: () => {
            this.repositoryForm.get('name').setValue(this.repo.name);
            this.repositoryForm.get('owner').setValue(this.repo.owner);
            this.repositoryForm.get('description').setValue(this.repo.description);
          }
        })
    );
  }

  loadUsers() {
    this.subscriptions.push(
      this.userService.getAllUsers()
        .subscribe(
          (users) => this.users = users
        )
    );

    if (this.isNew) {
      this.repositoryForm.get('owner').setValue(this.authService.currentUserValue.id);
      // assign current user as owner and disable the field
    }
  }

  onAddRepository() {
    if (this.repositoryForm.invalid) {
      return;
    }
    const name = this.repositoryForm.get('name').value;
    const owner = this.repositoryForm.get('owner').value;
    const description = this.repositoryForm.get('description').value;

    this.subscriptions.push(this.repositoryService.addNew(name, owner, description)
      .subscribe(() => this.created.emit())
    );
  }

  onUpdateRepository() {

  }

  onDeleteRepository() {

  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.repositoryForm.reset();
  }

}
