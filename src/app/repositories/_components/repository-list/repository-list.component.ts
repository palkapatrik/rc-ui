import { Component, OnInit } from '@angular/core';
import {RepositoryService} from '../../repository.service';
import {Repository} from '@app/_interfaces/repository';

@Component({
  selector: 'app-repository-list',
  templateUrl: './repository-list.component.html',
  styleUrls: ['./repository-list.component.sass']
})
export class RepositoryListComponent implements OnInit {
  repositoryList: Repository[] = [];

  detailVisible = false;
  detailId: string;
  detailName: string;

  constructor(private rs: RepositoryService) { }

  getAllRepos() {
    this.rs.listAllRepos()
      .subscribe(
        repos => this.repositoryList = repos
    );
  }

  showDetail(id: string, name: string) {
    this.detailId = id;
    this.detailName = name;
    this.detailVisible = true;
  }

  resetDetail() {
    this.detailId = '';
    this.detailName = '';
  }

  ngOnInit(): void {
    this.getAllRepos();
  }

}
