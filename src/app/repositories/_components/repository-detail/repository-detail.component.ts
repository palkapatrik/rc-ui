import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {RepositoryService} from '@app/repositories/repository.service';
import {Repository} from '@app/_interfaces/repository';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-repository-detail',
  templateUrl: './repository-detail.component.html',
  styleUrls: ['./repository-detail.component.sass']
})
export class RepositoryDetailComponent implements OnInit, OnDestroy {
  @Input() id: string;
  @Input() showName = true;
  @Input() showActions = false;

  subscriptions: Subscription[] = [];

  repo: Repository;

  constructor(private rs: RepositoryService) {
  }

  ngOnInit(): void {
    this.loadDetail();
  }

  loadDetail() {
    this.subscriptions.push(this.rs.getDetail(this.id)
      .subscribe(
        repository => this.repo = repository
      )
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
