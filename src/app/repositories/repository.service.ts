import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environment/environment';
import {Repository} from '@app/_interfaces/repository';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  constructor(private http: HttpClient) {
  }

  listAllRepos(): Observable<Repository[]> {
    return this.http.get<Repository[]>(`${environment.api}/repositories/`);
  }

  getDetail(id: string): Observable<Repository> {
    return this.http.get<Repository>(`${environment.api}/repositories/${id}`);
  }

  addNew(name: string, owner: string, description: string): Observable<Repository> {
    const newRepository: Repository = {
      name, owner, description
    };
    return this.http.post<Repository>(`${environment.api}/repositories/add`, newRepository);
  }
  //
  // archiveRepo(id: string) {
  //   // TODO archive repository
  // }
  //
  // deleteRepo(id: string) {
  //   // TODO delete repository
  // }
}
