import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RepositoryListComponent} from './_components/repository-list/repository-list.component';
import {RepositoryEditComponent} from './_components/repository-edit/repository-edit.component';

const routes: Routes = [
  {
    path: '',
    component: RepositoryListComponent,
    data: {
      title: 'Repositories'
    }
  },
  {
    path: 'add',
    component: RepositoryEditComponent,
    data: {
      title: 'New repository'
    }
  },
  {
    path: 'edit/:id',
    component: RepositoryEditComponent,
    data: {
      title: 'Edit repository'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepositoriesRoutingModule {
}
