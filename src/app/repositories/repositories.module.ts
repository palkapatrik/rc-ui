import {NgModule} from '@angular/core';
import {RepositoryListComponent} from './_components/repository-list/repository-list.component';
import {RepositoriesRoutingModule} from './repositories-routing.module';
import {CardModule} from 'primeng/card';
import {SharedModule} from '../shared/shared.module';
import {DialogModule} from 'primeng/dialog';
import {RepositoryDetailComponent} from './_components/repository-detail/repository-detail.component';
import {ButtonModule} from 'primeng/button';
import {RepositoryEditComponent} from './_components/repository-edit/repository-edit.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {DropdownModule} from 'primeng/dropdown';


@NgModule({
  declarations: [
    RepositoryListComponent,
    RepositoryDetailComponent,
    RepositoryEditComponent,
  ],
  exports: [
    RepositoryEditComponent
  ],
  imports: [
    SharedModule,
    RepositoriesRoutingModule,
    CardModule,
    DialogModule,
    ButtonModule,
    ReactiveFormsModule,
    InputTextModule,
    InputTextareaModule,
    DropdownModule,
  ]
})
export class RepositoriesModule {
}
