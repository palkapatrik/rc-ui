import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from '@app/_helpers/auth.guard';
import {LoginFormComponent} from '@app/_components/login-form/login-form.component';
import {DashboardComponent} from '@app/_components/dashboard/dashboard.component';
import {SignupFormComponent} from '@app/_components/signup-form/signup-form.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginFormComponent,
    data: {
      title: 'Login to continue'
    }
  },
  {
    path: 'signup',
    component: SignupFormComponent,
    data: {
      title: 'Create new account'
    }
  },
  {
    path: 'dashboard', component: DashboardComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Dashboard'
    }
  },
  {
    path: 'repositories',
    loadChildren: () => import('./repositories/repositories.module').then(m => m.RepositoriesModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'releases',
    loadChildren: () => import('./releases/releases.module').then(m => m.ReleasesModule),
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: 'dashboard',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
