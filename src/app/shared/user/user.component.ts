import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '@app/_services/user.service';
import {User} from '@app/_interfaces/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass']
})
export class UserComponent implements OnInit {
  @Input() userId: string;
  user: User;

  constructor(private us: UserService) {
  }

  getDetail(): void {
    this.us.getUserById(this.userId)
      .subscribe(
        u => this.user = u
      );
  }

  ngOnInit(): void {
    this.getDetail();
  }

}
