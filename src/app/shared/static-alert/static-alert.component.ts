import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-static-alert',
  template: `
    <p-message [severity]="severity"
               [text]="content">
    </p-message>
  `
})
export class StaticAlertComponent implements OnInit {
  @Input() severity: 'warn' | 'error' | 'info' | 'success';
  @Input() content: string;

  constructor() { }

  ngOnInit(): void {
  }

}
