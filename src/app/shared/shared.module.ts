import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserComponent} from './user/user.component';
import {SpinnerComponent} from './spinner/spinner.component';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {StaticAlertComponent} from './static-alert/static-alert.component';
import {MessageModule} from 'primeng/message';
import { PopupWindowComponent } from './popup-window/popup-window.component';
import {DialogModule} from 'primeng/dialog';


@NgModule({
  declarations: [
    UserComponent,
    SpinnerComponent,
    StaticAlertComponent,
    PopupWindowComponent
  ],
  imports: [
    CommonModule,
    OverlayPanelModule,
    ProgressSpinnerModule,
    MessageModule,
    DialogModule
  ],
  exports: [
    CommonModule,
    UserComponent,
    SpinnerComponent,
    StaticAlertComponent,
    PopupWindowComponent,
  ]
})
export class SharedModule {
}
