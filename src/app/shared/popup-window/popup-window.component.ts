import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-popup-window',
  templateUrl: './popup-window.component.html',
})
export class PopupWindowComponent implements OnInit {
  @Input() header: string;
  @Input() isVisible = false;

  constructor() {
  }

  ngOnInit(): void {
  }
}
