export interface Repository {
  id?: string;
  name: string;
  description: string;
  owner: string;
  versions?: string[];
  lastRelease?: string;
}
