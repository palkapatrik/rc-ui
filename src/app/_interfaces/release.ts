import {ReleaseStates} from '../_enums/release-states.enum';

export interface Release {
  id?: string;
  name: string;
  creator: string;
  state: ReleaseStates;

  // when the release concept was created
  created: string;
  // when the concept was sent
  sent?: string;
  // when it should be deployed
  plannedDeployment?: string;
  // when it actually was deployed
  deployed?: string;

  // included services
  services: string[];

  // db changes
  databaseSchemas: string[];
  databaseComment?: string;

  // additional changes
  additionalChanges?: string;
}
