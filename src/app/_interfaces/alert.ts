import {AlertSeverity} from '../_enums/alert-severity.enum';

export interface Alert {
  id: string;
  severity: AlertSeverity;
  summary: string;
  detail: string;
  life?: number;
  closeable?: boolean;
}
