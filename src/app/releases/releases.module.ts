import { NgModule } from '@angular/core';
import { ReleaseListComponent } from './_components/release-list/release-list.component';
import { NewReleaseComponent } from './_components/new-release/new-release.component';
import {ReleasesRoutingModule} from './releases-routing.module';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  imports: [
    SharedModule,
    ReleasesRoutingModule,
  ],
  declarations: [ReleaseListComponent, NewReleaseComponent],
  exports: [
    NewReleaseComponent
  ]
})
export class ReleasesModule { }
