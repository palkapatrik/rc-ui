import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReleaseListComponent} from './_components/release-list/release-list.component';
import {NewReleaseComponent} from './_components/new-release/new-release.component';

const routes: Routes = [
  {
    path: '',
    component: ReleaseListComponent,
    data: {
      title: 'Releases'
    }
  },
  {
    path: 'new',
    component: NewReleaseComponent,
    data: {
      title: 'New release'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReleasesRoutingModule {
}
