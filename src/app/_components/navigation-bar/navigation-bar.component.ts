import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/api';
import {AuthService} from '@app/_services/auth.service';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
})
export class NavigationBarComponent implements OnInit {
  items: MenuItem[] = [
    {label: 'Dashboard', icon: 'pi pi-fw pi-home', routerLink: '/dashboard'},
    {label: 'Releases', icon: 'pi pi-fw pi-send', routerLink: '/releases'},
    {label: 'Repositories', icon: 'pi pi-fw pi-folder', routerLink: '/repositories'},
    {
      label: 'More', items: [
        {label: 'Edit user', icon: 'pi pi-fw pi-pencil', command: () => this.openUserEdit()},
        {label: 'Log out', icon: 'pi pi-fw pi-sign-out', command: () => this.logout()}
      ]
    },
    {
      label: 'Create',
      icon: 'pi pi-fw pi-plus',
      items: [
        {label: 'New Release', icon: 'pi pi-fw pi-plus', command: () => this.showReleaseModal = true},
        {label: 'Add repository', icon: 'pi pi-fw pi-plus', command: () => this.showRepositoryModal = true}
      ]
    }
  ];

  activeItem: MenuItem;

  showReleaseModal = false;
  showRepositoryModal = false;

  userAuthenticated = false;

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.userAuthenticated = !!this.authService.currentUserValue;

    this.authService.currentUser
      .subscribe(() => {
        this.userAuthenticated = !!this.authService.currentUserValue;
      });
  }

  openUserEdit() {
    alert('open user edit');
  }

  logout() {
    this.authService.logout();
  }

}
