import {Component} from '@angular/core';
import {Alert} from '@app/_interfaces/alert';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.sass']
})
export class AlertComponent {
  alerts: Alert[];

  constructor() {
  }
}
