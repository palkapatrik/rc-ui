import {Component, OnInit} from '@angular/core';
import {AuthService} from '@app/_services/auth.service';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginFormComponent implements OnInit {
  loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(private authService: AuthService,
              private router: Router,
              private fb: FormBuilder) {
  }

  onSubmit() {
    if (this.loginForm.valid) {
      const email = this.loginForm.get('email').value;
      const password = this.loginForm.get('password').value;

      this.authService.login(email, password)
        .subscribe(() => {
          this.router.navigate(['/']);
        });
    }
  }

  ngOnInit(): void {
  }

}
