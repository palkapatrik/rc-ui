import { Component, OnInit } from '@angular/core';
import {AuthService} from '@app/_services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  constructor(private authService: AuthService) { }

  getActiveUserName() {
    return this.authService.currentUserValue.fullName;
  }

  getActiveUserEmail() {
    return this.authService.currentUserValue.email;
  }

  ngOnInit(): void {
  }

}
