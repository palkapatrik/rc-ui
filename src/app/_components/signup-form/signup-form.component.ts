import {Component, OnInit} from '@angular/core';
import {AuthService} from '@app/_services/auth.service';
import {User} from '@app/_interfaces/user';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html'
})
export class SignupFormComponent implements OnInit {
  signupForm = this.fb.group({
    fullName: ['', Validators.required],
    email: ['', Validators.email],
    username: ['', Validators.minLength(3)],
    password: ['', Validators.minLength(3)]
  });

  constructor(private authService: AuthService,
              private router: Router,
              private fb: FormBuilder) {
  }

  validateForm() {
    if (this.signupForm.valid) {
      this.signUp({
        fullName: this.signupForm.get('fullName').value,
        email: this.signupForm.get('email').value,
        password: this.signupForm.get('password').value
      });
    }
  }

  signUp(user: User) {
    this.authService.signup(user)
      .subscribe(() => {
        this.router.navigate(['/login']);
      });
  }

  ngOnInit(): void {
  }

}
