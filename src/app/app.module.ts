import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AlertComponent} from './_components/alert/alert.component';
import {ToastModule} from 'primeng/toast';
import {NavigationBarComponent} from './_components/navigation-bar/navigation-bar.component';
import {DashboardComponent} from './_components/dashboard/dashboard.component';
import {LoginFormComponent} from './_components/login-form/login-form.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {JwtInterceptor} from '@app/_helpers/jwt.interceptor';
import {ErrorInterceptor} from '@app/_helpers/error.interceptor';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {ConfirmPopupModule} from 'primeng/confirmpopup';
import {MenuModule} from 'primeng/menu';
import {SignupFormComponent} from './_components/signup-form/signup-form.component';
import {DialogModule} from 'primeng/dialog';
import {MenubarModule} from 'primeng/menubar';
import {TabMenuModule} from 'primeng/tabmenu';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {SharedModule} from '@app/shared/shared.module';
import {RepositoriesModule} from '@app/repositories/repositories.module';
import {ReleasesModule} from '@app/releases/releases.module';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';


@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    NavigationBarComponent,
    DashboardComponent,
    LoginFormComponent,
    SignupFormComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    CardModule,
    ButtonModule,
    FormsModule,
    InputTextModule,
    ConfirmPopupModule,
    MenuModule,
    DialogModule,
    MenubarModule,
    TabMenuModule,
    ReactiveFormsModule,
    OverlayPanelModule,
    SharedModule,
    RepositoriesModule,
    ReleasesModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
  ],
  exports: [
    AlertComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
